import rsa


async def bob(websocket, path, stop_request):
    (B_pubkey, B_privkey) = rsa.newkeys(1024, poolsize=8)  # генерация ключей Боба
    print("Getting public key...")
    alice_pubkey = rsa.PublicKey.load_pkcs1(await websocket.recv(), format='DER')  # получение открытого ключа Алисы
    print("Sending public key...")
    await websocket.send(B_pubkey.save_pkcs1(format='DER'))  # отправка открытого ключа Боба
    alice_msg = await websocket.recv()  # ожидание сообщения Алисы
    print("Receiving message: " + str(alice_msg) + "...")
    input_answer = input("Input answer: ")
    answer = rsa.encrypt(input_answer.encode('utf-8'), alice_pubkey)
    first = answer[:len(answer) // 2]
    second = answer[len(answer) // 2:]
    await websocket.send(first)
    print("Sent first half")
    alice_msg = alice_msg + await websocket.recv()
    alice_msg = rsa.decrypt(alice_msg, B_privkey)  # Боб расшифровывает сообщение Алисы своим закрытым ключом
    print("Recieved message: " + str(alice_msg)[2:-1])
    await websocket.send(second)  # Боб отправляет вторую половину своего зашифрованного сообщения
    print("Sent second half")
    stop_request.set()
