import asyncio
import functools
import websockets

from Alice import alice
from Bob import bob


async def main():
    exit_switch = 0
    while exit_switch == 0:
        switch = input('Who are you? (Alice/Bob): ')
        if switch == 'Bob':
            stop_request = asyncio.Event()
            alice_handler = functools.partial(bob, stop_request=stop_request)
            server = await websockets.serve(alice_handler, "localhost", 1234)
            await stop_request.wait()
            stop_request.clear()
            server.close()
        elif switch == 'Alice':
            await alice()
        elif switch == 'exit':
            exit_switch = 1


if __name__ == '__main__':
    asyncio.run(main())
