import websockets
import rsa


async def alice():
    (A_pubkey, A_privkey) = rsa.newkeys(1024, poolsize=8)  # генерация ключей Алисы
    bob = await websockets.connect("ws://localhost:1234")
    print("Sending public key...")
    await bob.send(A_pubkey.save_pkcs1(format='DER'))  # отправка открытого ключ Алисы
    print("Getting public key...")
    bob_pub_key = rsa.PublicKey.load_pkcs1(await bob.recv(), format='DER')  # получение открытого ключа Боба
    input_msg = input('Input message: ')
    message = rsa.encrypt(input_msg.encode('utf-8'), bob_pub_key)  # Алиса шифрует сообщение открытым ключом Боба
    first = message[:len(message) // 2]
    second = message[len(message) // 2:]
    await bob.send(first)  # Алиса отправляет первую половину
    print("Sent first half")
    bobmsg = await bob.recv()  # Получаем первую половину сообщения Боба
    print("Recieving message: " + str(bobmsg) + "...")
    await bob.send(second)  # Алиса отправляет вторую половину
    print("Sent second half")
    bobmsg = bobmsg + await bob.recv()  # Получение второй половины сообщения Боба
    bobmsg = rsa.decrypt(bobmsg, A_privkey)
    print("Recieved message: " + str(bobmsg)[2:-1])
