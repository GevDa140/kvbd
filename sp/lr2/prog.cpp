#include <iostream>
using namespace std;

int main()
{
	unsigned char arr[12] = {238, 42, 134, 168, 4, 85, 219, 171, 182, 15, 106, 226};
	int count = 0;
	unsigned char mask = 17;
	unsigned char prom1;
	unsigned char prom2;
	for (int i = 0; i < 12; i++)
	{
		prom1 = arr[i] ^ mask;
		prom2 = prom1 & mask;
		if (prom2 == mask)
			count++;
	}
	cout << count << endl;
	return 0;
}
