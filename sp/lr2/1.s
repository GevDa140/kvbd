#1.s
.data
msg:
	.ascii "abcd text\n"
	#.byte 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x0A #aaaaaaaaa
	len = .-msg

.text
	.global _start
_start:
	movl $4, %eax
	movl $1, %ebx
	movl $msg, %ecx
	movl $len, %edx
	int $0x80

	movl $1, %eax
	xorl %ebx, %ebx
	int $0x80
