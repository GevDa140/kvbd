#!/bin/bash
#
echo "Программа для создания образа CD-диска"
echo "С помощью данной программы вы можете создать образ CD-диска с содержимым из указанного каталога"
echo "Разработчик: Геворгян Давид Размикович"
isofile(){
echo "Введите название каталога"
read dir
if [ -d "$dir" ]
then
 echo "Введите название файла"
 read name
 format=`date +"%F"`
 nf="${name}${format}"
 iso="${name}.iso"
 if [ -f "$iso" ]
 then
  genisoimage -o ${nf}.iso $dir
  ex
 else
  genisoimage -o $name.iso $dir
  ex
 fi
else
 echo "Каталога не существует"
 ex
fi
}
ex(){
echo "Хотите продолжить выполнение программы? (y/n)"
read flag
mark="y"
if [[ "$flag" == "$mark" ]];
then
isofile
else
exit
fi
}
while true; do
isofile
done
